package com.juliang.common;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 包量代理（企业版）删除代理IP白名单
 */
@Data
@Accessors(chain = true)
public class CompanyDynamicDelWhiteIp {
    /**
     * key 业务秘钥
     */
    private String key;
    /**
     * 业务号
     */
    private String trade_no;

    /**
     * 删除的代理IP白名单
     */
    private String del_ip;
}
