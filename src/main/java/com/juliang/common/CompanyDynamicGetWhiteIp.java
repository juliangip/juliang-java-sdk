package com.juliang.common;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 包量代理（企业版）---获取代理IP白名单
 */
@Data
@Accessors(chain = true)
public class CompanyDynamicGetWhiteIp {
    /**
     * key 业务秘钥
     */
    private String key;
    /**
     * 业务编号
     */
    private String trade_no;
}
