package com.juliang.common;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 按量付费（企业版）---获取代理IP
 */
@Data
@Accessors(chain = true)
public class CompanyPostPayGetIps {
    /**
     * key 业务秘钥
     */
    private String key;
    /**
     * 业务编号
     */
    private String trade_no;
    /**
     * 提取数量
     */
    private Integer num;
    /**
     * 代理类型
     */
    private Integer pt;
    /**
     * 返回类型
     */
    private String result_type;
    /**
     * 结果分隔符
     */
    private Integer split;
    /**
     * 省份（只能指定一个）
     */
    private String province;
    /**
     * 城市（只能指定单一城市，当城市和省份不匹配时，以城市为先）
     */
    private String city;
    /**
     * 剩余可用时长
     */
    private Integer ip_remain;
    /**
     * IP去重
     */
    private Integer filter;
}
