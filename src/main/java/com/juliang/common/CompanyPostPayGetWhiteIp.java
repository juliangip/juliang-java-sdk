package com.juliang.common;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 按量付费---获取代理IP白名单
 */
@Data
@Accessors(chain = true)
public class CompanyPostPayGetWhiteIp {
    /**
     * key 业务秘钥
     */
    private String key;
    /**
     * 业务编号
     */
    private String trade_no;
}
