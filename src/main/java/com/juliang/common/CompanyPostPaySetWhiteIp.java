package com.juliang.common;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 按量付费（企业版）-------删除IP白名单
 */
@Data
@Accessors(chain = true)
public class CompanyPostPaySetWhiteIp {
        /**
     * key 业务秘钥
     */
    private String key;
    /**
     * 业务编号
     */
    private String trade_no;
    /**
     * IP列表
     */
    private String ips;
}
