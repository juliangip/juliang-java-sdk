package com.juliang.common;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 按量付费 -- 检查ip是否可用
 */
@Data
@Accessors(chain = true)
public class PostPayCheck {
    /**
     * key 业务秘钥
     */
    private String key;
    /**
     * 业务号
     */
    private String trade_no;
    /**
     * 代理列表
     */
    private String proxy;
}
