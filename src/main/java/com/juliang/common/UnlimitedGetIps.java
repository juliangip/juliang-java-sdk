package com.juliang.common;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UnlimitedGetIps {
    /**
     * trade_no 业务编号
     */
    private String trade_no;
    /**
     * key 业务秘钥
     */
    private String key;
    /**
     * 提取数量
     */
    private Integer num;
    /**
     * 代理类型
     */
    private Integer pt;
    /**
     * 返回类型
     */
    private String result_type;
    /**
     * 结果分隔符
     */
    private Integer split;
    /**
     * 地区名称
     */
    private Integer city_name;
    /**
     * 邮政编码
     */
    private Integer city_code;
    /**
     * 剩余可用时长
     */
    private Integer ip_remain;
}
