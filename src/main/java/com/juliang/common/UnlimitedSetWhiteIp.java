package com.juliang.common;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UnlimitedSetWhiteIp {
    /**
     * 业务密钥
     */
    private String key;

    /**
     * 业务编号
     */
    private String trade_no;

    /**
     * 白名单IP
     */
    private String ips;
}
