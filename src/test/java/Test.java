import cn.hutool.system.SystemUtil;
import com.juliang.JuLiang;
import com.juliang.common.*;

import java.util.Map;

public class Test {

    private String userId = "your User ID";
    private String AccessKey = "User AccessKey";

    private String dynamicKey = "dynamicKey";
    private String dynamicTradeNo = "dynamicTradeNo";

    private String aloneTradeNo = "aloneTradeNo";
    private String aloneKey = "aloneKey";

    private String postPayTradeNo = "postPayTradeNo";
    private String postPayKey = "postPayKey";

    private String unlimitedTradeNo = "unlimitedTradeNo";
    private String unlimitedKey = "unlimitedKey";

    private String companyPostPayTradeNo = "companyPostPayTradeNo";
    private String companyPostPayKey = "companyPostPayKey";

    private String companyDynamicTradeNo = "companyDynamicTradeNo";
    private String companyDynamicKey = "companyDynamicKey";

    /**
     * 测试获取账户余额API
     *
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void userBalance() throws IllegalAccessException {
        String money = JuLiang.usersGetBalance(new UsersGetBalance().setUser_id(userId).setKey(AccessKey));
        System.out.println(money);
    }

    @org.junit.jupiter.api.Test
    public void usersGetAllOrders() throws IllegalAccessException {
        String orderList = JuLiang.getAllOrders(new UsersGetAllOrders().setKey(AccessKey).setUser_id(userId).setShow("1").setProduct_type(4));
        System.out.println(orderList);
    }

    @org.junit.jupiter.api.Test
    public void usersGetCity() throws IllegalAccessException {
        String cityInfo = JuLiang.getCity(new UsersGetCity().setKey(AccessKey).setUser_id(userId).setProvince("山东,河北"));
        System.out.println(cityInfo);
    }

    @org.junit.jupiter.api.Test
    public void test() throws Exception {
        String a = JuLiang.dynamicGetIps(new DynamicGetIps().setTrade_no(dynamicTradeNo).setNum(1).setKey(dynamicKey));
        System.out.println(a);
        String[] as = a.split("\n");
        long begintime = System.currentTimeMillis();
        String resp = JuLiang.dynamicCheck(new DynamicCheck().setTrade_no(dynamicTradeNo).setKey(dynamicKey).setProxy(as[0]));
        long endintime = System.currentTimeMillis();
        System.out.println(endintime - begintime);
        System.out.println(resp);
    }

    /**
     * 动态代理 -- 提取代理iP
     *
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void dyGetIps() throws IllegalAccessException {
        String a = JuLiang.dynamicGetIps(new DynamicGetIps().setTrade_no(dynamicTradeNo).setNum(10).setKey(dynamicKey));
        System.out.println(a);
    }

    /**
     * 动态代理 -- 检查代理的有效性
     *
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void Check() throws IllegalAccessException {
        String resp = JuLiang.dynamicCheck(new DynamicCheck().setTrade_no(dynamicTradeNo).setKey(dynamicKey).setProxy("120.39.142.214:40786,153.37.115.56:57433,42.84.173.2:55228"));
        System.out.println(resp);
    }

    /**
     * 动态代理 -- 设置代理IP白名单
     *
     * @throws IllegalAccessException 返回实例 {"msg":"请求成功","code":200,"data":{"white_ip_count":5,"current_white_ip":["1.1.1.1","2.2.2.2","30.30.30.30"],"surplus_white_ip_quantity":2}}
     */
    @org.junit.jupiter.api.Test
    public void dySetWhiteIp() throws IllegalAccessException {
        String resp = JuLiang.dynamicSetWhiteIp(new DynamicSetWhiteIp().setTrade_no(dynamicTradeNo).setKey(dynamicKey).setIps("2.2.2.2,30.30.30.30"));
        System.out.println(resp);
    }

    /**
     * 动态代理 -- 获取代理IP白名单
     * <p>
     * 返回示例 {"msg":"成功","code":200,"data":{"white_ip_count":5,"current_white_ip":["1.1.1.1","2.2.2.2","30.30.30.30"],"surplus_white_ip_quantity":2}}
     *
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void dyGetWhiteIp() throws IllegalAccessException {
        String resp = JuLiang.dynamicGetWhiteIp(new DynamicGetWhiteIp().setTrade_no(dynamicTradeNo).setKey(dynamicKey));
        System.out.println(resp);
    }

    /**
     * 动态代理 -- 替换代理Ip白名单
     *
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void dyReplaceWhiteIp() throws IllegalAccessException {
        String resp = JuLiang.dynamicReplaceWhiteIp(new DynamicReplaceWhiteIp().setTrade_no(dynamicTradeNo).setKey(dynamicKey).setNew_ip("11.12.13.14,15.16.17.18").setOld_ip("3.3.3.9").setReset("1"));
        System.out.println(resp);
    }

    /**
     * 动态代理 -- 业务代理IP剩余数量
     *
     * @throws IllegalAccessException 返回示例 {"msg":"成功","code":200,"data":{"balance":4999880}}
     */
    @org.junit.jupiter.api.Test
    public void balance() throws IllegalAccessException {
        String resp = JuLiang.dynamicBalance(new DynamicBalance().setTrade_no(dynamicTradeNo).setKey(dynamicKey));
        System.out.println(resp);
    }

    /**
     * 动态代理 -- 获取代理剩余可用时长
     *
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void dynamicremain() throws IllegalAccessException {
        String value = JuLiang.dynamicRemain(new DynamicRemain().setTrade_no(dynamicTradeNo).setKey(dynamicKey).setProxy("1.1.1.1:8082"));
        System.out.println(value);
    }


    /**
     * 独享代理 -- 获取白名单
     *
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void alonegetwhiteip() throws IllegalAccessException {
        String value = JuLiang.aloneGetWhiteIp(new AloneGetWhiteIp().setTrade_no(aloneTradeNo).setKey(aloneKey));
        System.out.println(value);
    }

    /**
     * 独享代理 -- 设置IP白名单
     *
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void setwhiteip() throws IllegalAccessException {
        String value = JuLiang.aloneSetWhiteIp(new AloneSetWhiteIp().setTrade_no(aloneTradeNo).setKey(aloneKey).setIps("10.10.10.10"));
        System.out.println(value);
    }

    /**
     * 独享代理 -- 获取独享代理详情
     *
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void alonegetips() throws IllegalAccessException {
        String value = JuLiang.aloneGetIps(new AloneGetIps().setTrade_no(aloneTradeNo).setKey(aloneKey));
        System.out.println(value);
    }

    /**
     * 独享代理 -- 替换代理IP白名单
     *
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void aloneReplaceWhiteIp() throws IllegalAccessException {
        String resp = JuLiang.aloneReplaceWhiteIp(new AloneReplaceWhiteIp().setTrade_no(aloneTradeNo).setKey(aloneKey).setNew_ip("11.12.13.14,15.16.17.18").setOld_ip("3.3.3.9").setReset("1"));
        System.out.println(resp);
    }

    /**
     * 按量付费 -- 提取ip
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void postPayGetIps() throws IllegalAccessException{
        String value = JuLiang.postPayGetIps(new PostPayGetIps().setTrade_no(postPayTradeNo).setKey(postPayKey).setNum(10));
        System.out.println(value);
    }

    /**
     * 按量付费 -- 检查ip是否可用
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void postPayCheck() throws IllegalAccessException{
        String value = JuLiang.postPayCheck(new PostPayCheck().setTrade_no(postPayTradeNo).setKey(postPayKey).setProxy("223.6.6.6:33225"));
        System.out.println(value);
    }

    /**
     * 按量付费 -- 设置ip白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void postPaySetWhiteIp() throws IllegalAccessException {
        String value = JuLiang.postPaySetWhiteIp(new PostPaySetWhiteIp().setTrade_no(postPayTradeNo).setKey(postPayKey).setIps("223.6.6.6"));
        System.out.println(value);
    }

    /**
     * 按量付费 -- 获取ip白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void postPayGetWhiteIp() throws IllegalAccessException {
        String value = JuLiang.postPayGetWhiteIp(new PostPayGetWhiteIp().setTrade_no(postPayTradeNo).setKey(postPayKey));
        System.out.println(value);
    }

    /**
     * 按量付费 -- 替换ip白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void postPayReplaceWhiteIp() throws IllegalAccessException {
        String value = JuLiang.postPayReplaceWhiteIp(new PostPayReplaceWhiteIp().setTrade_no(postPayTradeNo).setKey(postPayKey).setReset("1").setNew_ip("223.6.6.6"));
        System.out.println(value);
    }

    /**
     * 不限量 -- 获取ip
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void unlimitedGetIps() throws IllegalAccessException{
        String value = JuLiang.unlimitedGetIps(new UnlimitedGetIps().setTrade_no(unlimitedTradeNo).setKey(unlimitedKey).setNum(10));
        System.out.println(value);
    }

    /**
     * 不限量 -- 设置白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void unlimitedSetIps() throws IllegalAccessException{
        String value = JuLiang.unlimitedSetWhiteIp(new UnlimitedSetWhiteIp().setTrade_no(unlimitedTradeNo).setKey(unlimitedKey).setIps("223.6.6.6"));
        System.out.println(value);
    }

    /**
     * 不限量 -- 获取白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void unlimitedGetWhiteIp() throws IllegalAccessException{
        String value = JuLiang.unlimitedGetWhiteIp(new UnlimitedGetWhiteIp().setTrade_no(unlimitedTradeNo).setKey(unlimitedKey));
        System.out.println(value);
    }

    /**
     * 不限量 -- 替换白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void unlimitedReplaceWhiteIp() throws IllegalAccessException {
        String value = JuLiang.unlimitedReplaceWhiteIp(new UnlimitedReplaceWhiteIp().setTrade_no(unlimitedTradeNo).setKey(unlimitedKey).setOld_ip("223.6.6.6").setNew_ip("223.6.6.6"));
        System.out.println(value);
    }

    /**
     * 按量付费(企业版) -- 提取ip
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void CompanyPostPayGetIps() throws IllegalAccessException{
        String value = JuLiang.CompanyPostPayGetIps(new CompanyPostPayGetIps().setTrade_no(companyPostPayTradeNo).setKey(companyPostPayKey).setNum(1));
        System.out.println(value);
    }

    /**
     * 按量付费 -- 设置ip白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void CompanyPostPaySetWhiteIp() throws IllegalAccessException {
        String value = JuLiang.CompanyPostPaySetWhiteIp(new CompanyPostPaySetWhiteIp().setTrade_no(companyPostPayTradeNo).setKey(companyPostPayKey).setIps("223.6.6.6"));
        System.out.println(value);
    }

    /**
     * 按量付费 -- 获取ip白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void CompanyPostPayGetWhiteIp() throws IllegalAccessException {
        String value = JuLiang.CompanyPostPayGetWhiteIp(new CompanyPostPayGetWhiteIp().setTrade_no(companyPostPayTradeNo).setKey(companyPostPayKey));
        System.out.println(value);
    }

    /**
     * 按量付费 -- 替换ip白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void CompanyPostPayDelWhiteIp() throws IllegalAccessException {
        String value = JuLiang.CompanyPostPayDelWhiteIp(new CompanyPostPayDelWhiteIp().setTrade_no(companyPostPayTradeNo).setKey(companyPostPayKey).setDel_ip("223.6.6.6"));
        System.out.println(value);
    }

    /**
     * 包时/包量(企业版) -- 提取ip
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void CompanyDynamicGetIps() throws IllegalAccessException{
        String value = JuLiang.CompanyDynamicGetIps(new CompanyDynamicGetIps().setTrade_no(companyDynamicTradeNo).setKey(companyDynamicKey).setNum(1));
        System.out.println(value);
    }

    /**
     * 包时/包量(企业版) -- 获取ip白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void CompanyDynamicGetWhiteIp() throws IllegalAccessException {
        String value = JuLiang.CompanyDynamicGetWhiteIp(new CompanyDynamicGetWhiteIp().setTrade_no(companyDynamicTradeNo).setKey(companyDynamicKey));
        System.out.println(value);
    }

    /**
     * 包时/包量(企业版) -- 设置ip白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void CompanyDynamicSetWhiteIp() throws IllegalAccessException {
        String value = JuLiang.CompanyDynamicSetWhiteIp(new CompanyDynamicSetWhiteIp().setTrade_no(companyDynamicTradeNo).setKey(companyDynamicKey).setIps("171.40.167.185"));
        System.out.println(value);
    }

    /**
     * 包时/包量(企业版) -- 删除ip白名单
     * @throws IllegalAccessException
     */
    @org.junit.jupiter.api.Test
    public void CompanyDynamicDelWhiteIp() throws IllegalAccessException {
        String value = JuLiang.CompanyDynamicDelWhiteIp(new CompanyDynamicDelWhiteIp().setTrade_no(companyDynamicTradeNo).setKey(companyDynamicKey).setDel_ip("171.40.167.185"));
        System.out.println(value);
    }
}
